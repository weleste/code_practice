class TomatoClock {
    constructor() {
        this.workTime = 25 * 60; // 默认25分钟
        this.shortBreak = 5 * 60; // 短休息5分钟
        this.longBreak = 15 * 60; // 长休息15分钟
        this.sessionsCompleted = 0;
        this.isRunning = false;
        this.timerId = null;
        this.currentTime = this.workTime;
        this.isWorkSession = true;

        this.initElements();
        this.initEvents();
        this.updateDisplay();
    }

    initElements() {
        this.minutesDisplay = document.getElementById('minutes');
        this.secondsDisplay = document.getElementById('seconds');
        this.startBtn = document.getElementById('start');
        this.pauseBtn = document.getElementById('pause');
        this.resetBtn = document.getElementById('reset');
        this.workTimeInput = document.getElementById('work-time');
        this.shortBreakInput = document.getElementById('short-break');
        this.longBreakInput = document.getElementById('long-break');
        this.completedSessions = document.getElementById('completed-sessions');
        this.longBreakBtn = document.getElementById('long-break-btn');
    }

    initEvents() {
        this.startBtn.addEventListener('click', () => this.startTimer());
        this.pauseBtn.addEventListener('click', () => this.pauseTimer());
        this.resetBtn.addEventListener('click', () => this.resetTimer());
        this.workTimeInput.addEventListener('change', (e) => this.updateWorkTime(e));
        this.shortBreakInput.addEventListener('change', (e) => this.updateShortBreak(e));
        this.longBreakInput.addEventListener('change', (e) => this.updateLongBreak(e));
        this.longBreakBtn.addEventListener('click', () => this.startLongBreak());
    }

    startTimer() {
        if (!this.isRunning) {
            this.isRunning = true;
            this.timerId = setInterval(() => this.tick(), 1000);
            this.startBtn.disabled = true;
            this.pauseBtn.disabled = false;
        }
    }

    pauseTimer() {
        if (this.isRunning) {
            clearInterval(this.timerId);
            this.isRunning = false;
            this.startBtn.disabled = false;
            this.pauseBtn.disabled = true;
        }
    }

    resetTimer() {
        this.pauseTimer();
        this.currentTime = this.isWorkSession ? this.workTime : this.shortBreak;
        this.updateDisplay();
    }

    tick() {
        if (this.currentTime > 0) {
            this.currentTime--;
            this.updateDisplay();
            this.updateBackground();
        } else {
            this.handleTimerComplete();
        }
    }

    handleTimerComplete() {
        this.pauseTimer();
        this.showNotification();
        this.playCompleteAnimation();
        
        if (this.isWorkSession) {
            this.sessionsCompleted++;
            this.completedSessions.textContent = this.sessionsCompleted;
            
            if (this.sessionsCompleted % 4 === 0) {
                this.startLongBreak();
            } else {
                this.startShortBreak();
            }
        } else {
            this.startWorkSession();
        }
    }

    startWorkSession() {
        this.isWorkSession = true;
        this.currentTime = this.workTime;
        this.updateDisplay();
        this.startTimer();
    }

    startShortBreak() {
        this.isWorkSession = false;
        this.currentTime = this.shortBreak;
        this.updateDisplay();
        this.startTimer();
    }

    startLongBreak() {
        this.isWorkSession = false;
        this.currentTime = this.longBreak;
        this.updateDisplay();
        this.startTimer();
    }

    updateDisplay() {
        const minutes = Math.floor(this.currentTime / 60);
        const seconds = this.currentTime % 60;
        this.minutesDisplay.textContent = String(minutes).padStart(2, '0');
        this.secondsDisplay.textContent = String(seconds).padStart(2, '0');
    }

    updateBackground() {
        const progress = 1 - (this.currentTime / (this.isWorkSession ? this.workTime : this.shortBreak));
        const hue = 120 + (240 * progress); // 从绿色渐变到红色
        document.body.style.backgroundColor = `hsl(${hue}, 50%, 95%)`;
    }

    showNotification() {
        if (Notification.permission === 'granted') {
            new Notification(this.isWorkSession ? '休息时间到！' : '工作时间到！', {
                body: this.isWorkSession ? '该休息一下了' : '该开始工作了',
                icon: 'https://example.com/icon.png'
            });
        }
    }

    playCompleteAnimation() {
        const display = document.querySelector('.timer-display');
        display.classList.add('complete-animation');
        display.addEventListener('animationend', () => {
            display.classList.remove('complete-animation');
        }, { once: true });

        // 添加粒子效果
        this.createConfetti();
    }

    createConfetti() {
        const container = document.createElement('div');
        container.classList.add('confetti-container');
        
        for (let i = 0; i < 100; i++) {
            const confetti = document.createElement('div');
            confetti.classList.add('confetti');
            confetti.style.left = `${Math.random() * 100}vw`;
            confetti.style.animationDelay = `${Math.random() * 2}s`;
            confetti.style.backgroundColor = `hsl(${Math.random() * 360}, 70%, 60%)`;
            container.appendChild(confetti);
        }

        document.body.appendChild(container);
        
        setTimeout(() => {
            container.remove();
        }, 2000);
    }

    updateDisplay() {
        const minutes = Math.floor(this.currentTime / 60);
        const seconds = this.currentTime % 60;
        
        // 添加数字过渡效果
        this.animateNumberChange(this.minutesDisplay, String(minutes).padStart(2, '0'));
        this.animateNumberChange(this.secondsDisplay, String(seconds).padStart(2, '0'));
    }

    animateNumberChange(element, newValue) {
        if (element.textContent !== newValue) {
            element.classList.add('changing');
            element.addEventListener('transitionend', () => {
                element.textContent = newValue;
                element.classList.remove('changing');
            }, { once: true });
        } else {
            element.textContent = newValue;
        }
    }

    updateWorkTime(e) {
        this.workTime = e.target.value * 60;
        if (!this.isRunning && this.isWorkSession) {
            this.currentTime = this.workTime;
            this.updateDisplay();
        }
    }

    updateShortBreak(e) {
        this.shortBreak = e.target.value * 60;
        if (!this.isRunning && !this.isWorkSession) {
            this.currentTime = this.shortBreak;
            this.updateDisplay();
        }
    }

    updateLongBreak(e) {
        this.longBreak = e.target.value * 60;
    }
}

// 初始化
document.addEventListener('DOMContentLoaded', () => {
    // 请求通知权限
    if (Notification.permission !== 'granted') {
        Notification.requestPermission();
    }

    new TomatoClock();
});