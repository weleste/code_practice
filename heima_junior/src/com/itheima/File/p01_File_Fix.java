package com.itheima.File;

import java.io.File;
import java.text.Format;

public class p01_File_Fix {
    public static void main(String[] args) {
        String pathname ="C:\\Users\\Azure\\Desktop\\6";
        File f1 =new File(pathname);
        System.out.println(pathname);

        String s="a.txt";
        File f3 = new File(f1,s);
        System.out.println(f3.getAbsolutePath());

        String s1="b.txt";
        File f4 = new File(s,s1);
        System.out.println(f4.getAbsolutePath());

        String s2="c.txt";
        File f5 = new File(f4,s2);
    }
}
