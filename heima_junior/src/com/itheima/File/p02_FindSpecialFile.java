package com.itheima.File;

import java.io.File;

public class p02_FindSpecialFile {
    public static void main(String[] args) {
        File file1 = new File("D://daima//aaa");
        boolean b = HaveTxt(file1);
        System.out.println(b);
        File file2 = new File("D://daima//bbb");
        boolean b1 = HaveTxt(file2);
        System.out.println(b1);

    }

    public static boolean HaveTxt(File file) {
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isFile() && f.getName().endsWith(".txt")) {
                return true;
            }
        }
        return false;
    }
}
