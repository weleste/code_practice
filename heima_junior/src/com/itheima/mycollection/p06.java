package com.itheima.mycollection;

import java.util.ArrayList;
import java.util.Arrays;

public class p06<E> {
    //泛型类
    Object [] ob = new Object[10];
    int size;       

    public boolean add(E e)
    {
        ob[size++] = e;
        return true;
    }

    public E get(int index){
        return (E)ob[index];
    }
    //重写方法,让输入变量名时,返回的不是地址
    @Override
    public String toString() {
        return Arrays.toString(ob);
    }
}
