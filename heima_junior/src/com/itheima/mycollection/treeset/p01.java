package com.itheima.mycollection.treeset;

import java.util.TreeSet;

public class p01 {
    /*建立5个学生对象,属性为姓名,年龄,语文,数学,英语成绩
    *按照总分从高到低输出到控制台
    * 如果总分一样,按照语文成绩从高到低输出到控制台
    * 如果语文成绩一样,按照数学成绩从高到低输出到控制台
    * 如果数学成绩一样,按照英语成绩从高到低输出到控制台
    * 如果英语成绩一样,按照年龄从高到低输出到控制台
    * 如果年龄一样,按照姓名的字母顺序输出到控制台
    * 如果都一样,认为是同一个学生,不存
    * */

    public static void main(String[] args) {
        student s1 = new student("张三", 23, 90, 99, 50);
        student s2 = new student("李四", 24, 90, 98, 50);
        student s3 = new student("王五", 25, 95, 100, 30);
        student s4 = new student("赵六", 26, 60, 99, 70);
        student s5 = new student("钱七", 26, 70, 80, 70);

        TreeSet<student>ts = new TreeSet<>();
        ts.add(s1);
        ts.add(s2);
        ts.add(s3);
        ts.add(s4);
        ts.add(s5);

        //打印集合
        for (student t : ts) {
            System.out.println(t);
        }
    }
}
