package com.itheima.mycollection.treeset;

public class student implements Comparable<student>{
    private String name;
    private int age;
    private int chinesescore;
    private int mathscore;
    private int englishscore;


    public student() {
    }

    public student(String name, int age, int chinesescore, int mathscore, int englishscore) {
        this.name = name;
        this.age = age;
        this.chinesescore = chinesescore;
        this.mathscore = mathscore;
        this.englishscore = englishscore;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * 获取
     * @return chinesescore
     */
    public int getChinesescore() {
        return chinesescore;
    }

    /**
     * 设置
     * @param chinesescore
     */
    public void setChinesescore(int chinesescore) {
        this.chinesescore = chinesescore;
    }

    /**
     * 获取
     * @return mathscore
     */
    public int getMathscore() {
        return mathscore;
    }

    /**
     * 设置
     * @param mathscore
     */
    public void setMathscore(int mathscore) {
        this.mathscore = mathscore;
    }

    /**
     * 获取
     * @return englishscore
     */
    public int getEnglishscore() {
        return englishscore;
    }

    /**
     * 设置
     * @param englishscore
     */
    public void setEnglishscore(int englishscore) {
        this.englishscore = englishscore;
    }

    public String toString() {
        return "student{name = " + name + ", age = " + age + ", chinesescore = " + chinesescore + ", mathscore = " + mathscore + ", englishscore = " + englishscore + "}";
    }
    /*建立5个学生对象,属性为姓名,年龄,语文,数学,英语成绩
     *按照总分从高到低输出到控制台
     * 如果总分一样,按照语文成绩从高到低输出到控制台
     * 如果语文成绩一样,按照数学成绩从高到低输出到控制台
     * 如果数学成绩一样,按照英语成绩从高到低输出到控制台
     * 如果英语成绩一样,按照年龄从高到低输出到控制台
     * 如果年龄一样,按照姓名的字母顺序输出到控制台
     * 如果都一样,认为是同一个学生,不存
     * */
    @Override
    public int compareTo(student o) {
        int sum1 = this.chinesescore + this.mathscore + this.englishscore;
        int sum2 = o.chinesescore + o.mathscore + o.englishscore;
        int i=sum1-sum2;
        i=i==0?this.getChinesescore()-o.getChinesescore():i;
        i=i==0?this.getMathscore()-o.getMathscore():i;
        i=i==0?this.getEnglishscore()-o.getEnglishscore():i;
        i=i==0?this.getAge()-o.getAge():i;
        i=i==0?this.getName().compareTo(o.getName()):i;

        return i;
    }
}
