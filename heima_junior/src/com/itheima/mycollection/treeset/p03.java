package com.itheima.mycollection.treeset;

import java.util.TreeSet;

public class p03 {
    public static void main(String[] args) {
        //tree中字符串排序,先按长度排序,若长度相同,则按首字母顺序排序
        TreeSet<String> ts=new TreeSet<>( (o1,  o2)-> {
              int i = o1.length()-o2.length();
              i=i==0?o1.compareTo(o2):i;
              return i;

        });
        ts.add("a");

        ts.add("bc");
        ts.add("df");
        ts.add("awer");

        for (String t : ts) {
            System.out.println(t);
        }

    }

    }
