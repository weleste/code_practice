package com.itheima.mycollection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class p05 {
    public static void main(String[] args) {
        //列表迭代器
        List<String> list=new ArrayList<>();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        System.out.println(list);
        System.out.println("-------------------------");
        //删除元素有两种方法
        list.remove("bbb");
        list.remove(1);
        ListIterator<String>it =list.listIterator();
        while(it.hasNext()) {
            String s = it.next();
            if(s.equals("aaa")) {
                it.add("666");
            }
        }
        System.out.println(list);
    }

}
