package com.itheima.mycollection;

import javafx.beans.binding.ListExpression;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

public class p04 {

    public static void main(String[] args) {
        //增强for遍历
        Collection<String> coll = new ArrayList<>();
        coll.add("zhangsan");
        coll.add("lisi");
        coll.add("wangwu");

        for (String s : coll) {
            System.out.println(s);
        }
        System.out.println("-------------------------------");

        //lambda表达式
        coll.forEach(s -> System.out.println(s));

        //common for
        for (int i = 0; i < coll.size(); i++) {
            String str = ((ArrayList<String>) coll).get(i);
        }



    }
}

