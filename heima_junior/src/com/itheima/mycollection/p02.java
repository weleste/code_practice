package com.itheima.mycollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class p02 {
    //contains方法中,底层会调用object方法中的equals方法比较地址值
    //若要比较属性值,记得再JavaBean中重写方法
    public static void main(String[] args) {
        Collection<student> coll = new ArrayList<>();

        student s1 = new student("zhangsan", 23);
        student s2 = new student("lisi", 24);
        student s3 = new student("wangwu", 25);

        coll.add(s1);
        coll.add(s2);
        coll.add(s3);


        student s4 = new student("zhangsan", 23);

        //  System.out.println(coll.contains(s4));

        System.out.println(coll.contains(s4));

        //遍历
        //迭代器好比箭头,默认指向集合的零索引处
        Iterator<student> it = coll.iterator();
        while (it.hasNext()) {
            student s = it.next();
            System.out.println(s);
        }
        //指针不恢复位
        //System.out.println(it.next());

        //集合的遍历

    }
}
