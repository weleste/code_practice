package com.itheima.mycollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class p01 {
    public static void main(String[] args) {
        //collection是接口,不可直接创建对象,可用Array List实现
        Collection<String> coll = new ArrayList<>();

        //添加元素
        coll.add("aaa");
        coll.add("bbb");
        coll.add("ccc");
        coll.add("ddd");
        System.out.println(coll);

        //删除,定义的是共性的方法,只能通过元素删去,而非索引
        System.out.println(coll.remove("aaa"));
        System.out.println(coll);

        //判断包含
        System.out.println(coll.contains("aaa"));
        System.out.println(coll.contains("bbb"));

        //集合长度
        System.out.println(coll.size());

        //判断是否为空
        System.out.println(coll.isEmpty());

        //清空
        coll.clear();
        System.out.println(coll);


    }
}
