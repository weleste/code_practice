package com.itheima.Twice;

import java.util.Locale;

public class p01_DataType {
    public static void main(String[] args) {
        String s1 ="HELLO".toLowerCase();
        String s2 ="hello";
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s1.equals(s2));
        System.out.println(s1==s2);
    }
}
