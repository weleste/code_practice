package com.itheima.Twice;

public class p05_CanShuChuanDi {// 基本类型参数绑定

        public static void main(String[] args) {
            Person p = new Person();
            int n = 15; // n的值为15
            p.setAge(n); // 传入n的值
            System.out.println(p.getAge()); // 15
            n = 20; // n的值改为20
            System.out.println(p.getAge()); // 15还是20?
        }

    static class Person {
        private int age;

        public int getAge() {
            return this.age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
