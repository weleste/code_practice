package com.itheima.Twice;

import java.util.Arrays;

public class P03_ArrayToString {
    public static void main(String[] args) {
        int[] ns = { 1, 1, 2, 3, 5, 8 };
        System.out.println(Arrays.toString(ns));
        //该方法参数为一个数组,返回值会自动加上[]
    }
}
