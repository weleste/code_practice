package com.itheima.Exception;

public class User {
    private String name;
    private int age;

    public User() {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        //名字长度2-9个字符
            return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) throws nameformexception {
        if (name.length() < 2 || name.length() > 9) {
            throw new nameformexception("name length must be 2-9");
        }else {
            this.name = name;
        }
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        if (age < 18 || age > 40 ){
            throw new AgeOutOfBoundsException("age must be 18-40");
        }else{
        this.age = age;}
    }

    public String toString() {
        return "User{name = " + name + ", age = " + age + "}";
    }
}
