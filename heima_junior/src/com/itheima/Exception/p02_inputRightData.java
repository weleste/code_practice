package com.itheima.Exception;

import java.util.Scanner;

public class p02_inputRightData {
    public static void main(String[] args) {
        //判断键盘录入的信息,要求符合一定的格式
        Scanner s = new Scanner(System.in);
        User u = new User();
        while (true) {

            try{
                System.out.println("please input your name:");
                String name = s.nextLine();
                    u.setName(name);

                System.out.println("please input your age:");
                int age = Integer.parseInt(s.nextLine());
                u.setAge(age);
                break;

            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (nameformexception n) {
                n.printStackTrace();
            }catch (AgeOutOfBoundsException a){
                a.printStackTrace();
            }
        }

        System.out.println(u);
    }
}
