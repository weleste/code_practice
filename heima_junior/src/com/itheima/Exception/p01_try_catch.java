package com.itheima.Exception;

public class p01_try_catch {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        System.out.println(arr[1]);

        try {
            System.out.println(arr[10]);
        } catch (ArrayIndexOutOfBoundsException e) {
            String message = e.toString();
            System.out.println(message);
          //e.printStackTrace();
        }

        System.out.println("看看我执行了吗?");
    }
}
