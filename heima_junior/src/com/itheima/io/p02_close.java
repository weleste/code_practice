package com.itheima.io;

import java.io.FileOutputStream;
import java.io.IOException;

public class p02_close {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("p01//a.txt");
        fos.write(97);
        //fos.close();
        //未释放对象引用,该对象一直存在内存中
        while (true) {
        }
    }
}
