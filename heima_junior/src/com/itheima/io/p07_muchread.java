package com.itheima.io;

import java.io.FileInputStream;
import java.io.IOException;

public class p07_muchread {
    public static void main(String[] args) throws IOException {
            FileInputStream fis= new FileInputStream("p01//a.txt");
        byte [] bytes =new byte[9];
        int len1;
        len1 =fis.read(bytes);
        System.out.println(len1);
        String s1= new String(bytes,0,len1);
        System.out.println(s1);
        int len2;
        len2 =fis.read(bytes);
        System.out.println(len2);
        String s2= new String(bytes,0,len2);
        System.out.println(s2);
    }
}
