package com.itheima.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class p06_compareCopy {
    public static void main(String[] args) throws IOException {
        FileInputStream fis =new FileInputStream("D://大数据1班//一些作业//image.png");
        FileOutputStream fos = new FileOutputStream("p01//b.png");
        long start = System.currentTimeMillis();
        byte []bytes =new byte[1024*1024];
        int len;
        while ((len=fis.read(bytes))!=-1){
            fos.write(bytes,0,len);
        }
        fos.close();
        fis.close();
        long end =System.currentTimeMillis();
        System.out.println(end-start);
    }
}
