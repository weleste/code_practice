package com.itheima.io;

import java.io.*;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class p17_zipStream {
    public static void main(String[] args) throws IOException {
        //创建两个file表示压缩的代码源和目的地
        File src = new File("D:\\daima\\abc.zip");
        File des = new File("D:\\daima\\abc");
        unzip(src, des);
    }

    //创建一个方法实现解压缩
    public static void unzip(File src, File des) throws IOException {
        //创建压缩流
        ZipInputStream zip = new ZipInputStream(new FileInputStream(src));
        //获取当中每个文件或者文件夹(感觉类似listfile
        ZipEntry entry;
        while ((entry = zip.getNextEntry()) != null) {
            System.out.println(entry);
            //如果是文件夹,则在目的地创建一个同名的文件夹
            if (entry.isDirectory()) {
                File file = new File(des, entry.toString());
                file.mkdirs();
            } else {
                //如果是文件,则要读取压缩包中的文件,并把他放到目的地中的文件夹
                FileOutputStream fos = new FileOutputStream(new File(des, entry.toString()));
                int a;
                while ((a = zip.read()) != -1) {
                    fos.write(a);
                }
                fos.close();
                //表示在压缩包中的一个文件处理完了
                zip.closeEntry();
            }
        }
        zip.close();
    }
}
