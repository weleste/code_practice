package com.itheima.io;

import java.io.*;

public class p15_obxuliehua {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //创建反序列对象
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("p01\\xulie.txt"));
        //获取对象
        Object o = ois.readObject();
        //打印输出并关闭流
        System.out.println(o);
        ois.close();
    }
}
