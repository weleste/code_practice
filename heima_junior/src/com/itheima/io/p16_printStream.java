package com.itheima.io;

import java.io.*;

public class p16_printStream {
    public static void main(String[] args) throws FileNotFoundException {
        PrintStream ps = new PrintStream(new FileOutputStream("p01\\print.txt"));
        ps.println("hello");
        ps.print("world");
        ps.printf("%s何时照我还","明月");
        ps.println();
        ps.close();
        PrintWriter pw = new PrintWriter(new FileOutputStream("p01\\print.txt",true),false);
        pw.println("今天你叫我的名字了,虽然叫错了");
        pw.print("但是没关系");
        pw.printf("%s","我马上回去改");
        pw.close();

    }
}
