package com.itheima.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class p05_copy {
    public static void main(String[] args) throws IOException {
        FileInputStream fis =new FileInputStream("D://大数据1班//一些作业//image.png");
        FileOutputStream fos =new FileOutputStream("p01//a.png");
        long start = System.currentTimeMillis();
        int a ;
        while((a =fis.read()) !=-1){
            fos .write(a);
        }
        //先开后关
        fos.close();
        fis.close();
        long end = System.currentTimeMillis();
        System.out.println(end-start);
    }
}
