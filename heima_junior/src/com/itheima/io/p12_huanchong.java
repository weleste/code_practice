package com.itheima.io;

import java.io.*;

public class p12_huanchong {
    public static void main(String[] args) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream("p01\\a"));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("p01\\copy.txt"));
        int len;
        while ((len=bis.read())!= -1){
            bos.write(len);
        }
        bos.close();
        bis.close();
    }
}
