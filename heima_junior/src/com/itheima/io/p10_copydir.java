package com.itheima.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class p10_copydir {
    public static void main(String[] args) throws IOException {
        //拷贝文件,考虑子文件夹
        //创建对象表示数据源
        File scr = new File("p01");
        //创建对象表示目的地
        File dest = new File("D:\\daima");
        //创建方法拷贝
        copydir(scr, dest);
    }

    //参数一:要拷贝的文件
    //参数二:拷贝的目的地
    private static void copydir(File scr, File dest) throws IOException {

        //进入数据源
        File[] files = scr.listFiles();
        //遍历数组
        for (File file : files) {
            //1.是文件则直接拷贝
            if (file.isFile()) {
                FileInputStream fis = new FileInputStream(file);
                FileOutputStream fos = new FileOutputStream(new File(dest, file.getName()));
                byte[] bytes = new byte[1024];
                int len;
                while ((len = fis.read(bytes)) != -1) {
                    fos.write(bytes, 0, len);
                }
                fos.close();
                fis.close();
            } else {
                //2.不是文件则递归
                copydir(file, new File(dest, file.getName()));
            }
        }


    }
}
