package com.itheima.io;

import java.io.*;
import java.util.Arrays;

public class p11_paixu {
    public static void main(String[] args) throws IOException {
        //读取文件
        FileInputStream fis = new FileInputStream("p01\\c.txt");
        StringBuilder sb = new StringBuilder();
        int ch;
        while ((ch = fis.read()) != -1) {
            sb.append((char) ch);
        }
        fis.close();
        System.out.println(sb);
        //排序
        Integer[] arr = Arrays.stream(sb.toString().split("-"))
                .map(Integer::parseInt)
                .sorted()
                .toArray(Integer[]::new);
        //输出
        FileWriter fw = new FileWriter("p01\\d.txt");
        String str  = Arrays.toString(arr).replace(", ","-");
        String result = str.substring(1, str.length() - 1);
        fw.write(result);
        System.out.print(result);
        fw.close();
    }
}
