package com.itheima.io;

import sun.nio.cs.ext.GBK;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class p08_BianMaYuJieMa {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String s ="Ming月何时照我还";
        byte [] bytes1 = s.getBytes();
        byte [] bytes2 = s.getBytes("gbk");

        String s1 =new String(Arrays.toString(bytes1));
        String s2 =new String(Arrays.toString(bytes2));

        //打印出来的仍然是编码数字
        System.out.println(s1);
        System.out.println(s2);

//        语法：String str = new String(byte[] bytes);
//        作用：使用平台的默认字符集将字节数组解码为字符串。
        String l1 =new String(bytes1);
        String l2 =new String(bytes2,"gbk");

        System.out.println(l1);
        System.out.println(l2);
    }
}
