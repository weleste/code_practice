package com.itheima.io;

import java.io.FileOutputStream;
import java.io.IOException;

public class p03_changeAndRewrite {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos =new FileOutputStream("p01//a.txt",true
        );
        String str ="Hello";
        byte[] bytes = str.getBytes();
        fos.write(bytes);

        String str2 = "\r\n";
        byte[] bytes2 = str2.getBytes();
        fos.write(bytes2);

        String str3 ="World";
        byte[] bytes3 = str3.getBytes();
        fos.write(bytes3);

        fos.close();


    }
}
