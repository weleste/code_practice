package com.itheima.io;

import java.io.*;

public class p13_zhuanhuanliu {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("p01\\a");
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        System.out.println(s);
        br.close();

        System.out.println("-----------------------------------");
        BufferedReader br1 = new BufferedReader(new InputStreamReader(new FileInputStream("p01\\a.txt")));
        String s1 = br1.readLine();
        System.out.println(s1);
    }
}
