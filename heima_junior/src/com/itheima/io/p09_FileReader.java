package com.itheima.io;

import java.io.FileReader;
import java.io.IOException;

public class p09_FileReader {
    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("p01\\a");
        int ch;
        while ((ch = fr.read())!=-1)
        {
            System.out.print((char)ch);
        }
        fr.close();
        System.out.println();
        System.out.println("-----------------------------");

        FileReader fr1 =new FileReader("p01\\a");
        char [] chars =  new char[3];
        int len;
        while ((len=fr1.read(chars))!=-1) {
            System.out.print(new String(chars, 0, len));
        }
        fr1.close();
    }
}
