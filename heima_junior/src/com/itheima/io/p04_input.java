package com.itheima.io;

import java.io.FileInputStream;
import java.io.IOException;

public class p04_input {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("p01//a.txt");
        int str = fis.read();
        System.out.println(str);
        //read()调用一次,则指针移动一次
        int a;
        while((a=fis.read())!=-1){
            System.out.print((char)a);
        }
        fis.close();
    }
}
