package com.itheima.io;

import java.io.*;

public class p14_xulieliu {
    public static void main(String[] args) throws IOException {
        //创建对象
        students s = new students("张三",23);
        //转换成高级流
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("p01\\xulie.txt"));
        //写出数据
        oos.writeObject(s);
        //关闭流
        oos.close();

    }
}
