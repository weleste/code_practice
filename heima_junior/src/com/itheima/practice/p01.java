package com.itheima.practice;

import java.util.*;

public class p01 {
    //随机点名
    public static void main(String[] args) {
        ArrayList<String>al=new ArrayList<>();
        Collections.addAll(al,"张三","李四","王五","赵六","钱七");

        //随机数方式
        Random r=new Random();
        int name =r.nextInt(al.size());
        System.out.println(al.get(name));

        System.out.println("------------------------");

        //集合本身方法
        Collections.shuffle(al);
        System.out.println(al.get(0));

    }
}
