package com.itheima.yinyongfangfa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.function.IntFunction;

public class p01 {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<>();
        Collections.addAll(al, "1", "2", "3", "4", "5");
        //change them to integer
        al.stream().map(Integer::parseInt).forEach(s -> System.out.println(s));
        System.out.println("--------------------");
        Integer[] array = al.stream().map(Integer::parseInt).toArray(new IntFunction<Integer[]>() {
            @Override
            public Integer[] apply(int value) {
                return new Integer[value];
            }
        });

        System.out.println(Arrays.toString(array));


    }
}
