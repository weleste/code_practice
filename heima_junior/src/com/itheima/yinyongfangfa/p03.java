package com.itheima.yinyongfangfa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class p03 {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<>();
        Collections.addAll(al, "张无忌", "周芷若", "赵敏", "张强", "张三丰");
        //get some names which start of zhang and the length is 3
        al.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.startsWith("张") && s.length() == 3;
            }
        }).forEach(s -> System.out.println(s));
        System.out.println("--------------------");
        al.stream()
                .filter(s -> s.startsWith("张") && s.length() == 3)
                .forEach(s -> System.out.println(s));
        System.out.println("--------------------");
        al.stream().filter( new StringOperation ()::StringJudge)
                .forEach(s-> System.out.println(s));
    }


}
