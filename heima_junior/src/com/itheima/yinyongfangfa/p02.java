package com.itheima.yinyongfangfa;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Function;

public class p02 {
    public static void main(String[] args) {
        ArrayList <String> list  = new ArrayList<>();
        Collections.addAll(list,"aaa","bbb","ccc");
        list.stream().map(new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s.toUpperCase();
            }
        }).forEach(s->System.out.println(s));
System.out.println("--------------------");
list.stream().map(String::toUpperCase).forEach(s->System.out.println(s));
    }
}
