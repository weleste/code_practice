package com.itheima.zhengze;

public class p01 {
    public static void main(String[] args) {
        //用户名要求:大小写字母,数字,下划线一共4-16位
        String regex1 = "\\w{4,16}";
        System.out.println("zhangsan".matches(regex1));
        System.out.println("lisi_Joy34".matches(regex1));
        System.out.println("wangweu$".matches(regex1));

        System.out.println("-----------------------------------------------");
        //身份证号码验证:18位,前17为任意数字 最后一位可以是大小写X或数字
        String regex2 = "[1-9]\\d{16}(\\d|x|X)";
        //    String regex2 = "[1-9]\\d{16}[\\dXx]";
        //    String regex2 = "[1-9]\\d{16}(?i)x";

        System.out.println("41080119930228457x".matches(regex2));
        System.out.println("510801197609022309".matches(regex2));
        System.out.println("15040119817885432X".matches(regex2));
        System.out.println("998270846856789274".matches(regex2));
        System.out.println("99827084685678927d".matches(regex2));

        System.out.println("----------------------------------------------");
        //忽略大小写
        String regex3 = "a((?i)b)c";
        System.out.println("abc".matches(regex3));
        System.out.println("aBc".matches(regex3));
        System.out.println("aBC".matches(regex3));

        System.out.println("-----------------------------------------------");

        /*身份证号的验证
        前六位中第一位不能是0
        年的前半段18 19 20 21
        年的后半段:任意数字
        月份:01-09,11,12
        日期:01-09,10-19,20-29 30 31
        后四位:任意数字出现三次,最后一位还可以是大小写的X
         */
        String regex = "[1-9]\\d{5}(1[89]|2[012])\\d{2}(0[1-9]|11|12)(0[1-9]|[1-2]\\d|3[01])\\d{3}[\\d(?i)x]";
        System.out.println("41080119930228457x".matches(regex));
        System.out.println("510801197609022309".matches(regex));
        System.out.println("15040119817885432X".matches(regex));
        System.out.println("998270846856789274".matches(regex));
        System.out.println("99827084685678927d".matches(regex));

        //插件any_rule
String regax5= "[1-9]\\d{5}(?:18|19|20)\\d{2}(?:0[1-9]|10|11|12)(?:0[1-9]|[1-2]\\d|30|31)\\d{3}[\\dXx]";
    }
}
