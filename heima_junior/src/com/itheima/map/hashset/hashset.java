package com.itheima.map.hashset;

import java.util.HashSet;
import java.util.LinkedHashSet;

public class hashset {
    public static void main(String[] args) {
        //使姓名与年龄相同的元素无法再次添加

        //创建学生对象
        student s1=new student("zhangsan",23);
        student s2=new student("lisi",24);
        student s3=new student("wangwu",25);
        student s4=new student("zhangsan",23);


        //创建HashSet集合
        HashSet<student>hs=new HashSet<>();

        //添加元素
       System.out.println(hs.add(s1));
       System.out.println(hs.add(s2));
       System.out.println(hs.add(s3));
       System.out.println(hs.add(s4));

        System.out.println(hs);

       System.out.println("---------------------");
        //linkedhashset的存取数据有序性
        LinkedHashSet lhs = new LinkedHashSet();

        System.out.println(lhs.add(s1));
       System.out.println(lhs.add(s2));
       System.out.println(lhs.add(s3));
       System.out.println(lhs.add(s4));


       System.out.println(lhs);

    }
}
