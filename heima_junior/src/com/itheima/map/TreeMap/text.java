package com.itheima.map.TreeMap;

import java.util.TreeMap;

public class text {
    public static <s> void main(String[] args) {
        //查籍贯
        //创建TreeMap
        TreeMap<student, String> tm = new TreeMap<>();

        //创建学生对象
        student s1 = new student("zhangsan", 23);
        student s2 = new student("lisi", 24);
        student s3 = new student("wangwu", 25);

        //添加值
        tm.put(s1, "江苏");
        tm.put(s2, "上海");
        tm.put(s3, "北京");

       //for
        for (student s : tm.keySet()) {
            System.out.println(s.getName() + " " + s.getAge() + " " + tm.get(s));
        }

        System.out.println("---------------------");

        //lambda遍历
        tm.forEach(( student,  s)-> System.out.println(student.getName() + " " + student.getAge() + " " + s));
        
    }

}
