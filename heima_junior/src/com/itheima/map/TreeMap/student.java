package com.itheima.map.TreeMap;

public class  student implements Comparable<student>{
    private String name;
   private int age;

    public student() {
    }

    public student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "student{name = " + name + ", age = " + age + "}";
    }



    @Override
    public int compareTo(student o) {
        //按年龄排序,同则按名字字母顺序排序,同姓名同年龄视为同一个人
        int i =this.getAge()-o.getAge();
        i = i == 0 ? i = this.getName().compareTo(o.getName()) : i;
        return i;
    }
}
