package com.itheima.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class p02 {
    public static void main(String[] args) {
        Map<String, String> m = new HashMap<>();
        m.put("wangmo", "shuiwangzi");
        m.put("chensisi", "gaotaiming");
        m.put("baiguangying", "pangzueng");
        //get every key
        Set<String> k = m.keySet();

        //增强for遍历
        System.out.println(k);
        for (String key : k) {
            System.out.println(key + "=" + m.get(key));
        }
        System.out.println("---------------------");
        //迭代器遍历
        Iterator<String> it = k.iterator();
        while (it.hasNext()) {
            String key = it.next();
            System.out.println(key + "=" + m.get(key));
        }
        System.out.println("---------------------");
        //lambada表达式遍历
        k.forEach((s) -> System.out.println(s + "=" + m.get(s)));


    }
}
