package com.itheima.Thread;

public class p01_MyThread_test {
    public static void main(String[] args) {
        p01_Mythread mt1 = new p01_Mythread();
        p01_Mythread mt2 = new p01_Mythread();

        mt1.setName("Thread1:");
        mt2.setName("Thread2:");

        mt1.start();
        mt2.start();
    }
}
