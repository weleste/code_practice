package com.itheima.Thread;

public class foodie extends Thread {
    @Override
    public void run() {
        //循环
        while(true){
            //同步代码块
            synchronized (desk.lock) {
                //判断共享数据是否到了末尾
                if (desk.count == 0) {
                    break;
                } else {
                    if (desk.foodflag == 0) {
                        try {
                            desk.lock.wait();
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    } else {
                        desk.count--;
                        System.out.println("还能吃" + desk.count);
                        desk.lock.notifyAll();
                        desk.foodflag = 0;
                    }
                }
            }
        }
    }

}
