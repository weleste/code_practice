package com.itheima.Thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class p03_MyCallable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        p03_MyCallable_test mc =new p03_MyCallable_test();
        FutureTask ft = new FutureTask(mc);
        Thread t = new Thread(ft);
        t.start();
        System.out.println(ft.get());
    }
}
