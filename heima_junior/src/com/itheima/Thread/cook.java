package com.itheima.Thread;

public class cook extends Thread {
    @Override
    public void run() {
        synchronized (desk.lock) {
            while (true) {
                if (desk.count == 0) {
                    break;
                } else {
                    if (desk.foodflag == 0) {
                        System.out.println("cooking");
                        desk.foodflag = 1;
                        desk.lock.notifyAll();
                    } else {
                        try {
                            desk.lock.wait();
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }

            }
        }
    }
}
