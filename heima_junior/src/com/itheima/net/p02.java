package com.itheima.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class p02 {
    public static void main(String[] args) throws SocketException {
        //create and bind a datagram socket to a specific port
        DatagramSocket ds = new DatagramSocket(10086);

        byte[] bytes = new byte[1024];
        DatagramPacket dp = new DatagramPacket(bytes, bytes.length);

        try {
            ds.receive(dp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        byte[] data = dp.getData();
        int len = dp.getLength();
        InetAddress address = dp.getAddress();
        int port = dp.getPort();

        System.out.println("接收数据" + new String(data) + "从端口中来" + port);
        System.out.println("该数据是从" + address + "这台电脑中来的");

        ds.close();
    }
}
