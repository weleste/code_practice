package com.itheima.net;

import java.io.IOException;
import java.net.*;

public class p01 {
    public static void main(String[] args) throws SocketException, UnknownHostException {
        DatagramSocket ds = new DatagramSocket();

        String str = "春风又绿江南岸";
        byte[] bytes = str.getBytes();
        InetAddress address = InetAddress.getByName("127.0.0.1");
        int port = 10086;

        DatagramPacket dp = new DatagramPacket(bytes,bytes.length,address,10086);

        try {
            ds.send(dp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        ds.close();

    }
}

